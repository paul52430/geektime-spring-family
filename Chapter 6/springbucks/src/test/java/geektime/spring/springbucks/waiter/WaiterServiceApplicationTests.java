package geektime.spring.springbucks.waiter;

import com.github.rholder.retry.*;
import geektime.spring.springbucks.waiter.model.Coffee;
import geektime.spring.springbucks.waiter.model.CoffeeOrder;
import geektime.spring.springbucks.waiter.service.CoffeeOrderService;
import geektime.spring.springbucks.waiter.service.CoffeeService;
import geektime.spring.springbucks.waiter.service.ICoffeeService;
import geektime.spring.springbucks.waiter.support.CGLibRetryProxyHandler;
import geektime.spring.springbucks.waiter.support.RetryInvocationHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WaiterServiceApplicationTests {

	@Autowired
	private CoffeeService coffeeService;
	@Autowired
	private CoffeeOrderService coffeeOrderService;
	@Autowired
	private CGLibRetryProxyHandler cgLibRetryProxyHandler;

	@Qualifier("coffeeService")
	@Autowired
	private ICoffeeService iCoffeeService;

	@Test
	public void contextLoads() {
		Coffee latte = coffeeService.getCoffee("latte");
//		System.out.println(latte);
	}

	/**
	 * 手动重试重试
	 */
	@Test
	public void mannualRetry() throws InterruptedException {
		int times = 1;
		while (times <= 5) {
			try {
				// 故意抛异常
				int i = 3 / 0;
				Coffee latte = coffeeService.getCoffee("latte");
				// addOrder
			} catch (Exception e) {
				System.out.println("重试" + times + "次");
				Thread.sleep(2000);
				times++;
				if (times > 5) {
					throw new RuntimeException("不再重试！");
				}
			}
		}
	}

	/**
	 * 静态代理重试
	 */
	@Test
	public void staticProxy() {
		int times = 1;
		while (true) {
			try {
				// 故意抛异常
				int i = 3 / 0;
				Coffee latte = coffeeService.getCoffee("latte");
			} catch (Exception e) {
				System.out.println("重试" + times + "次");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
				times++;
				if (times > 5) {
					throw new RuntimeException("不再重试！");
				}
			}
		}
	}

	/**
	 * jdk动态代理重试
	 */
	@Test
	public void jdkProxy() {
		ICoffeeService proxy = (ICoffeeService) RetryInvocationHandler.getProxy(iCoffeeService);
		Coffee latte = proxy.getCoffee("latte");
		System.out.println(latte);
	}

	/**
	 * cglib动态代理重试
	 * 注意在使用cglib的时候，会不会产生一个内嵌的cglib代理，这样会报重复的cglib代理。
	 */
	@Test
	public void cglibProxy() {
		CoffeeOrderService proxy = (CoffeeOrderService)cgLibRetryProxyHandler.getCglibProxy(coffeeOrderService);
		CoffeeOrder order = proxy.get(1L);
		System.out.println(order);
	}

	/**
	 * 使用aop自定义重试
	 */
	@Test
	public void annotationRetry() {
		coffeeService.getCoffee(1L);
	}

	/**
	 * spring自带重试组件
	 * 注意：
	 * @Recover 注解标记的方法必须和被 @Retryable 标记的方法在同一个类中
	 * 重试方法抛出的异常类型需要与 recover() 方法参数类型保持一致
	 * recover() 方法返回值需要与重试方法返回值保证一致
	 * recover() 方法中不能再抛出 Exception，否则会报无法识别该异常的错误
	 * Spring的重试机制只支持对 异常 进行捕获，而无法对返回值进行校验
	 */
	@Test
	public void springRetry() {
		coffeeOrderService.get(1L);
	}

	/**
	 * guava重试组件：
	 * 先创建一个Retryer实例，然后使用这个实例对需要重试的方法进行调用，可以通过很多方法来设置重试机制：
	 *
	 * retryIfException()：对所有异常进行重试
	 * retryIfRuntimeException()：设置对指定异常进行重试
	 * retryIfExceptionOfType()：对所有 RuntimeException 进行重试
	 * retryIfResult()：对不符合预期的返回结果进行重试
	 * 还有五个以 withXxx 开头的方法，用来对重试策略/等待策略/阻塞策略/单次任务执行时间限制/自定义监听器进行设置，以实现更加强大的异常处理：
	 *
	 * withRetryListener()：设置重试监听器，用来执行额外的处理工作
	 * withWaitStrategy()：重试等待策略
	 * withStopStrategy()：停止重试策略
	 * withAttemptTimeLimiter：设置任务单次执行的时间限制，如果超时则抛出异常
	 */
	@Test
	public void guavaRetry() {
		Retryer<String> retryer = RetryerBuilder.<String>newBuilder()
				//无论出现什么异常，都进行重试
				.retryIfException()
				//返回结果为 error时，进行重试
				.retryIfResult(result -> Objects.equals(result, "error"))
				//重试等待策略：等待 2s 后再进行重试
				.withWaitStrategy(WaitStrategies.fixedWait(2, TimeUnit.SECONDS))
				//重试停止策略：重试达到 3 次
				.withStopStrategy(StopStrategies.stopAfterAttempt(3))
				.withRetryListener(new RetryListener() {
					@Override
					public <V> void onRetry(Attempt<V> attempt) {
						System.out.println("RetryListener: 第" + attempt.getAttemptNumber() + "次调用");
					}
				})
				.build();
		try {
			retryer.call(() -> String.valueOf(coffeeService.getCoffee("latte")));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}
