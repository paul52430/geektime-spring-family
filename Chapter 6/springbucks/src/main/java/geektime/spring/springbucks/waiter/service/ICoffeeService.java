package geektime.spring.springbucks.waiter.service;

import geektime.spring.springbucks.waiter.model.Coffee;
import org.joda.money.Money;

import java.util.List;

public interface ICoffeeService {

    Coffee saveCoffee(String name, Money price);

    List<Coffee> getAllCoffee();

    Coffee getCoffee(Long id);

    Coffee getCoffee(String name);

    List<Coffee> getCoffeeByName(List<String> names);

}
