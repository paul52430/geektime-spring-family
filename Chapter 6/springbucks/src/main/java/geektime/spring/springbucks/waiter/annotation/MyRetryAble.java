package geektime.spring.springbucks.waiter.annotation;

import java.lang.annotation.*;

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MyRetryAble {

    int retryTimes() default 3;

    int retryInterval() default 1;

}
