package geektime.spring.springbucks.waiter.support;

import geektime.spring.springbucks.waiter.annotation.MyRetryAble;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

/**
 * 使用aop重试
 */
@Slf4j
@Aspect
@Component
public class RetryAspect {

    @Pointcut("@annotation(geektime.spring.springbucks.waiter.annotation.MyRetryAble)")
    private void retryMethodCall() {

    }

    @Around("retryMethodCall()")
    public Object retry(ProceedingJoinPoint joinPoint) throws InterruptedException {
        // 获取重试次数和重试间隔
        MyRetryAble retry = ((MethodSignature)joinPoint.getSignature()).getMethod().getAnnotation(MyRetryAble.class);
        int maxRetryTimes = retry.retryTimes();
        int retryInterval = retry.retryInterval();

        Throwable error = new RuntimeException();
        for (int retryTimes = 1; retryTimes <= maxRetryTimes; retryTimes++){
            try {
                return joinPoint.proceed();
            } catch (Throwable throwable) {
                error = throwable;
                log.warn("调用发生异常，开始重试，retryTimes:{}", retryTimes);
            }
            Thread.sleep(retryInterval * 1000L);
        }
        throw new RuntimeException("重试次数耗尽", error);
    }

}
