package geektime.spring.cloud.eureka;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EurekaServerApplicationTests {

	@Test
	public void contextLoads() {
		System.out.println("hello world");

		int times = 1;
		while (times <= 5) {
			try {
				int i = 3 / 0;
				System.out.println("addOrder");
			} catch (Exception e) {
				System.out.println("重试" + times + "次");
				try {
					TimeUnit.SECONDS.sleep(5);
				} catch (InterruptedException exception) {
					exception.printStackTrace();
				}
				times++;
				if (times > 5) {
					throw new RuntimeException("不再重试");
				}
			}
		}

	}

}
