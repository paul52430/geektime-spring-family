package geektime.spring.springbucks.customer;

import geektime.spring.springbucks.customer.support.CustomConnectionKeepAliveStrategy;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import java.util.concurrent.TimeUnit;

@SpringBootApplication
@Slf4j
@EnableDiscoveryClient
@EnableFeignClients
public class CustomerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerServiceApplication.class, args);
	}

	/**
	 * HttpClient是Commons HttpClient的老版本，已被抛弃，不推荐使用；
	 * HttpClient是一个接口，定义了客户端HTTP协议的操作方法。
	 * 它可以用于发送HTTP请求和接收HTTP响应。
	 * HttpClient接口提供了很多方法来定制请求和处理响应。
	 * 这个接口存在于Apache HttpClient和Java自带的java.net包中。
	 * HttpClient没有close方法。无法关闭连接请求。
	 * HttpClient是单例模式，只能在一个应用中使用一个HttpClient实例；
	 *
	 * 与HttpClient相比，CloseableHttpClient具有以下几个优点：
	 *
	 * 1、支持连接池管理，即可复用已建立的连接；
	 * 2、自动管理连接释放；
	 * 3、支持GZIP解压；
	 * 4、支持HTTPS访问；
	 * 5、支持通用连接超时设置；
	 * 6、性能更优。
	 */
	@Bean
	public CloseableHttpClient httpClient() {
		return HttpClients.custom()
				.setConnectionTimeToLive(30, TimeUnit.SECONDS) //最大连接时间
				.evictIdleConnections(30, TimeUnit.SECONDS) //设置一个定时线程，定时清理闲置连接,可以将这个定时时间设置为 keep alive timeout 时间的一半以保证超时前回收
				.setMaxConnTotal(200) //最大连接数
				.setMaxConnPerRoute(20)
				.disableAutomaticRetries()
				.setKeepAliveStrategy(new CustomConnectionKeepAliveStrategy())
				.build();
	}
}
