package geektime.spring.hello.greeting;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
//@ConditionalOnClass(GreetingApplicationRunner.class)
public class GreetingAutoConfiguration {
    @Bean
//    @ConditionalOnMissingBean(GreetingApplicationRunner.class)
//    @ConditionalOnProperty(name = "greeting.enabled", havingValue = "true", matchIfMissing = false)
    public GreetingApplicationRunner greetingApplicationRunner() {
        return new GreetingApplicationRunner("AutoConfig");
    }
}
