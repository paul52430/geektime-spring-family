package geektime.spring.hello.greeting;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

@Slf4j
public class MyTestClass implements ApplicationRunner {

    public MyTestClass() {
        log.info("初始化MyTestClass");
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("MytestClass Runner! ");
    }
}
